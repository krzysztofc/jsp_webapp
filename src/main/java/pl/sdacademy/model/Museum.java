package pl.sdacademy.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Museum {

    private String id;
    private String name;
    private String postalCode;
    private String city;
    private String streetPrefix;
    private String street;
    private String houseNumber;
    private String flatNumber;
    private String organizer;
    private String entryDate;
    private String status;

}
