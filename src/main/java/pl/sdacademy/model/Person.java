package pl.sdacademy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private String name;
    private String lastName;
    private int bornYear;
    private int phoneNumber;
    private Sex sex;

    public Person(String name, String lastName, int bornYear, int phoneNumber) {
        this.name = name;
        this.lastName = lastName;
        this.bornYear = bornYear;
        this.phoneNumber = phoneNumber;
    }
}
