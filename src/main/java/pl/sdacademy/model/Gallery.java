package pl.sdacademy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Gallery {

    private String name;
    private List<Image> images;
    private final String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
//    private final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());


    public Gallery(String galleryName) {
        this.name = galleryName;
        this.images = new ArrayList<>();
    }

    public void addImage(String url, String description) {
        Image image = new Image(url, description);
        images.add(image);
    }
}
