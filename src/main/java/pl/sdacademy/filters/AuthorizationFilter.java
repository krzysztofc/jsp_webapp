package pl.sdacademy.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "AuthorizationFilter", urlPatterns = "/*")
public class AuthorizationFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        HttpSession session = request.getSession(false);
        String requestURI = request.getRequestURI();

        if ((session == null
                || session.getAttribute("user") == null)
                && !isLoginOrRegisterRequested(requestURI)) {
            log("User not logged in");
            response.sendRedirect("startPage.jsp");
        } else {
            chain.doFilter(req, resp);
        }

    }

    public void init(FilterConfig config) throws ServletException {

    }

    private void log(String info) {
        System.out.println("[AuthorizationFilter]: " + info);
    }

    private boolean isLoginOrRegisterRequested(String requestURI) {
        if (requestURI.contains("login")
                || requestURI.contains("registration")
                || requestURI.contains("startPage")) {
            return true;
        } else {
            return false;
        }
    }

}
