package pl.sdacademy.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

@WebFilter(filterName = "ServerLoggingFilter", urlPatterns = "/*")
public class ServerLoggingFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        System.out.println("************************************************************************************************************");
        String requestURI = request.getRequestURI();
        String fullAddress = request.getRequestURL().toString();
        String requestMethod = request.getMethod();
//        Map<String, String[]> parameterMap = request.getParameterMap();
//        for (Map.Entry s : parameterMap.entrySet()) {
//    }
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            log("Parameter name: " + entry.getKey() + ", value: " + Arrays.asList(entry.getValue()));
        }

        log("RequestURI: [" + requestMethod + "]: " + requestURI);
        log("Full request address: " + fullAddress);


        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

    private void log(String info) {
        System.out.println("[ServerLoggingFilter]" + info);
    }

}
