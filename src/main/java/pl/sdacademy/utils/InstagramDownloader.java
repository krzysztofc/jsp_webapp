package pl.sdacademy.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class InstagramDownloader {

    private String url = "https://www.instagram.com/_rl9/?hl=pl";
    private int numberOfPhotos;

    public InstagramDownloader(String url, int numberOfPhotos) {
        this.url = url;
        this.numberOfPhotos = numberOfPhotos;
    }

    public void downloadPhotos() {
        String stringJson = downloadSource();
        List<String> listOfPhotos = fetchJson(stringJson);
        writeToFile(listOfPhotos);


    }

    private String downloadSource() {
        String stringJson = null;
        try {
            URL url = new URL(this.url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String lineOfJson = bufferedReader.lines()
                    .filter(line -> line.contains("window._sharedData ="))
                    .findFirst()
                    .orElseGet(null);
            // limit 2 daje nam tablice z 2elementow
            String[] arrayOfLines = lineOfJson.split("window._sharedData =", 2);
            stringJson = arrayOfLines[1].replace(";<script>", "").trim();
            System.out.println(lineOfJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringJson;
    }

    private List<String> fetchJson(String stringJson) {
        List<String> listOfPhotosUrls = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(stringJson);
        JSONArray jsonArray = jsonObject.getJSONObject("entry_data")
                .getJSONArray("ProfilePage")
                .getJSONObject(0)
                .getJSONObject("graphql")
                .getJSONObject("user")
                .getJSONObject("edge_owner_to_timeline_media")
                .getJSONArray("edges");

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
            String photoURL = jsonObject1.getJSONObject("node")
                    .getString("display_url");
            listOfPhotosUrls.add(photoURL);
        }
        return listOfPhotosUrls;
    }

    private void writeToFile(List<String> photosUrls) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("C:\\Users\\Krzysztof\\Desktop\\JAVA OD PODSTAW\\JSP\\src\\main\\resources\\imagesFile"))) {
            for (String photosUrl : photosUrls) {
                bufferedWriter.write(photosUrl + "   @" + "photo\n");
//                System.out.println(photosUrl);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
