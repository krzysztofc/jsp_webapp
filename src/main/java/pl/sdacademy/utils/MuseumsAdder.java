package pl.sdacademy.utils;

import pl.sdacademy.model.Museum;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MuseumsAdder {

    private static List<Museum> museumsList = new ArrayList<>();

    private static List<Museum> populateMuseumsList() {

        String filePath = "C:\\Users\\Krzysztof\\Desktop\\JAVA OD PODSTAW\\JSP\\museums.csv";
        String line;

        try {
            FileReader fileReader = null;
            fileReader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            //pominiecie pierwszej lini
            bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null) {
                String[] split = line.split(";");
                Museum museum = new Museum(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8], split[9], split[10]);
                museumsList.add(museum);
            }
            fileReader.close();
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return museumsList;
    }

    public static void addMuseumsListToDataBase() {
        museumsList = populateMuseumsList();

        Connection connection = null;
        PreparedStatement preparedStatement;

        for (Museum museum : museumsList) {
            String id = museum.getId();
            String name = museum.getName();
            String postalCode = museum.getPostalCode();
            String city = museum.getCity();
            String streetPrefix = museum.getStreetPrefix();
            String street = museum.getStreet();
            String houseNumber = museum.getHouseNumber();
            String flatNumber = museum.getFlatNumber();
            String organizer = museum.getOrganizer();
            String entryDate = museum.getEntryDate();
            String status = museum.getStatus();

            try {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection("jdbc:sqlite:C:/Users/Krzysztof/Desktop/JAVA OD PODSTAW/SQLite/museums.db");
                preparedStatement = connection.prepareStatement("INSERT INTO museums " +
                        "(id, name, postalCode, city, streetPrefix, street, houseNumber, flatNumber, organizer, entryDate, status) " +
                        "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                preparedStatement.setString(1, id);
                preparedStatement.setString(2, name);
                preparedStatement.setString(3, postalCode);
                preparedStatement.setString(4, city);
                preparedStatement.setString(5, streetPrefix);
                preparedStatement.setString(6, street);
                preparedStatement.setString(7, houseNumber);
                preparedStatement.setString(8, flatNumber);
                preparedStatement.setString(9, organizer);
                preparedStatement.setString(10, entryDate);
                preparedStatement.setString(11, status);
                preparedStatement.executeUpdate();
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        addMuseumsListToDataBase();
    }
}
