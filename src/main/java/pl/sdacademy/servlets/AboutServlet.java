package pl.sdacademy.servlets;

import pl.sdacademy.model.Person;
import pl.sdacademy.model.Sex;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "AboutServlet", value = "/about")
public class AboutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Person> persons = new ArrayList<>();

        Person person1 = new Person("Krz", "Cie", 1990, 123456789, Sex.MALE);
        Person person2 = new Person("Tom", "Asz", 1991, 123456781,Sex.MALE);
        Person person3 = new Person("Pat", "Kos", 1992, 123456782,Sex.MALE);
        Person person4 = new Person("Ale", "Sander", 1996, 123456783,Sex.MALE);
        Person person5 = new Person("Ola", "Moja", 1989, 123456783,Sex.FEMALE);
        Person person6 = new Person("Zosia", "Twoja", 1980, 123456783,Sex.FEMALE);
        Person person7 = new Person("Basia", "Jego", 1983, 123456783,Sex.FEMALE);
        Person person8 = new Person("Kasia", "Jej", 1994, 123456783,Sex.FEMALE);

        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);
        persons.add(person5);
        persons.add(person6);
        persons.add(person7);
        persons.add(person8);


        List<Person> womanFirst = new ArrayList<>();
        for (Person woman : persons) {
            if (woman.getSex() == Sex.FEMALE) {
                womanFirst.add(woman);
            } else {
                continue;
            }
        }

        for (Person man : persons) {
            if (man.getSex() == Sex.MALE) {
                womanFirst.add(man);
            } else {
                continue;
            }
        }

        request.setAttribute("persons", persons);
        request.setAttribute("womanFirst", womanFirst);
        request.getRequestDispatcher("about.jsp").forward(request, response);

    }
}
