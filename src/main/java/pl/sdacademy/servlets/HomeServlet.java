package pl.sdacademy.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// value ="/home" - mapowanie na adres
@WebServlet(name = "HomeServlet", value = "/home")
public class HomeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String value = request.getParameter("action");
//        System.out.println("Parameter action: " + value);

        if (value == null) {
            response.sendRedirect("index.jsp");
        } else
            switch (value) {
                case "index":
                    response.sendRedirect("index.jsp");
                    break;
                case "about":
                    response.sendRedirect("about.jsp");
                    break;
                case "gallery":
                    response.sendRedirect("gallery.jsp");
                    break;
                case "contact":
                    response.sendRedirect("contact.jsp");
                    break;
                default:
                    response.sendRedirect("index.jsp");
            }
        System.out.printf("Redirecting to: %s.jsp, parameter value: %s\n",value,value);
    }
}


