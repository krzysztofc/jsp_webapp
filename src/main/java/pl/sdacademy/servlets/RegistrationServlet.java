package pl.sdacademy.servlets;

import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "RegistrationServlet", value = "/registration")
public class RegistrationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String inputLogin = request.getParameter("login");
        String inputPassword = DigestUtils.sha512Hex(request.getParameter("password"));

        Connection connection = null;
        PreparedStatement preparedStatementInput;
        PreparedStatement preparedStatementUsersInDB;
        ResultSet resultSet = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:C:/Users/Krzysztof/Desktop/JAVA OD PODSTAW/SQLite/users.db");
            preparedStatementUsersInDB = connection.prepareStatement("SELECT * FROM users WHERE login LIKE '" + inputLogin + "'");
            ResultSet resultSetWithUsersInDB = preparedStatementUsersInDB.executeQuery();

            if (resultSetWithUsersInDB.next()) {
                String errorMsg = "Użytkownik o takim loginie już istnieje";
                request.setAttribute("errorMsg", errorMsg);
                request.getRequestDispatcher("registration.jsp").forward(request, response);
            } else {
                preparedStatementInput = connection.prepareStatement("INSERT INTO users (login, password) VALUES (?,?)");
                preparedStatementInput.setString(1, inputLogin);
                preparedStatementInput.setString(2, inputPassword);
                preparedStatementInput.executeUpdate();
                String registrationMsg = "Użytkownik " + inputLogin + " został utworzony";
                request.setAttribute("registrationMsg", registrationMsg);
                request.getRequestDispatcher("index.jsp").forward(request, response);
                connection.close();
                preparedStatementInput.close();
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("registration.jsp");
    }
}
