package pl.sdacademy.servlets;

import pl.sdacademy.utils.InstagramDownloader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "InstagramServlet", value = "/instagram")
public class InstagramServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String number = request.getParameter("number");

        String url = "https://instagram.com/" + username;

        InstagramDownloader instagramDownloader = new InstagramDownloader(url, Integer.valueOf(number));
        instagramDownloader.downloadPhotos();
        response.sendRedirect("gallery");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("instagram.jsp");
    }
}
