package pl.sdacademy.servlets;

import pl.sdacademy.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "AddPersonServlet", value = "/addPerson")
public class AddPersonServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        Integer bornYear = Integer.valueOf(request.getParameter("bornYear"));
        Integer phone = Integer.valueOf(request.getParameter("phone"));
        String sex = request.getParameter("sex");

        Connection connection = null;
        PreparedStatement preparedStatement;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:C:/Users/Krzysztof/Desktop/JAVA OD PODSTAW/SQLite/people.db");
            preparedStatement = connection.prepareStatement("INSERT INTO people (name, surname, born_year, phone, sex) " +
                    "VALUES (?,?,?,?,?)");
            preparedStatement.setString(1,name);
            preparedStatement.setString(2,surname);
            preparedStatement.setInt(3,bornYear);
            preparedStatement.setInt(4,phone);
            preparedStatement.setString(5,sex);

            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        response.sendRedirect("showPersons");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("addPerson.jsp");
    }
}
