package pl.sdacademy.servlets;

import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "ChangePasswordServlet", value = "/changePassword")
public class ChangePasswordServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String inputLogin = request.getParameter("login");
        String inputPassword = DigestUtils.sha512Hex(request.getParameter("password"));
        String inputNewPassword = DigestUtils.sha512Hex(request.getParameter("newPassword"));
        String inputNewPasswordRepeted = DigestUtils.sha512Hex(request.getParameter("newPassword2"));

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:C:/Users/Krzysztof/Desktop/JAVA OD PODSTAW/SQLite/users.db");
            preparedStatement = connection.prepareStatement("SELECT password FROM users WHERE login LIKE'" + inputLogin + "'");
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            String passwordInDb = resultSet.getString(1);
            resultSet.close();
            if (passwordInDb.equals(inputPassword)) {
                if ((inputNewPassword.equals(inputNewPasswordRepeted)) && (!(inputNewPassword.equals(inputPassword)))) {
                    PreparedStatement preparedStatementUpdate = connection.prepareStatement("UPDATE users SET password='" + inputNewPassword + "' WHERE login LIKE '" + inputLogin + "'");
                    preparedStatementUpdate.executeUpdate();
                    preparedStatementUpdate.close();
                    String changedPassword = "Password has changed";
                    request.setAttribute("changedPassword", changedPassword);
                    request.getRequestDispatcher("index.jsp").forward(request,response);
                }else if (!(inputNewPassword.equals(inputNewPasswordRepeted)) && (inputNewPassword.equals(inputPassword))) {
                    String changedPassword = "New password has to be the same twice time";
                    request.setAttribute("changedPassword", changedPassword);
                    request.getRequestDispatcher("index.jsp").forward(request,response);
                } else if ((inputNewPassword.equals(inputNewPasswordRepeted)) && (inputNewPassword.equals(inputPassword))) {
                    String changedPassword = "New password has to be different from the old password";
                    request.setAttribute("changedPassword", changedPassword);
                    request.getRequestDispatcher("index.jsp").forward(request,response);
                }
            }else {
                String changedPasswordError = "Password has not been changed";
                request.setAttribute("changedPasswordError", changedPasswordError);
                request.getRequestDispatcher("changePassword.jsp").forward(request,response);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("changePassword.jsp");
    }
}
