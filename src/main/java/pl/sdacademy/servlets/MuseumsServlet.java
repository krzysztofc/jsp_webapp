package pl.sdacademy.servlets;

import pl.sdacademy.model.Museum;
import pl.sdacademy.model.Person;
import pl.sdacademy.model.Sex;
import pl.sdacademy.utils.MuseumsAdder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "MuseumsServlet", value = "/museums")
public class MuseumsServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String selectParam = (request.getParameter("select")).trim();
        String inputText = (request.getParameter("inputText")).trim();
        List<Museum> museumsList = new ArrayList<>();

        Connection connection = null;
        PreparedStatement preparedStatement;
        ResultSet resultSet = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection(
                    "jdbc:sqlite:C:/Users/Krzysztof/Desktop/JAVA OD PODSTAW/SQLite/museums.db");
            //%...% powoduja ze nie musimy podawac calego wyszukiwanego slowa np warszawa->warsz
            preparedStatement = connection.prepareStatement("SELECT * FROM museums WHERE " + selectParam + " LIKE '%" + inputText + "%' COLLATE NOCASE");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String id = resultSet.getString(1);
                String name = resultSet.getString(2);
                String postalCode = resultSet.getString(3);
                String city = resultSet.getString(4);
                String streetPrefix = resultSet.getString(5);
                String street = resultSet.getString(6);
                String houseNumber = resultSet.getString(7);
                String flatNumber = resultSet.getString(8);
                String organizer = resultSet.getString(9);
                String entryDate = resultSet.getString(10);
                String status = resultSet.getString(11);
                Museum museum = new Museum(id, name, postalCode, city, streetPrefix,
                        street, houseNumber, flatNumber, organizer, entryDate, status);
                museumsList.add(museum);
            }
            resultSet.close();
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("museumsList", museumsList);
        request.getRequestDispatcher("museums.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("museums.jsp");
    }
}
