package pl.sdacademy.servlets;

import pl.sdacademy.model.Person;
import pl.sdacademy.model.Sex;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ShowPersonsServlet", value = "/showPersons")
public class ShowPersonsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection connection = null;
        PreparedStatement preparedStatement;
        ResultSet resultSet = null;
        List<Person> personsList = new ArrayList<>();
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:C:/Users/Krzysztof/Desktop/JAVA OD PODSTAW/SQLite/people.db");
            preparedStatement = connection.prepareStatement("SELECT * FROM people");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString(1);
                String surname = resultSet.getString(2);
                int bornYear = resultSet.getInt(3);
                int phone = resultSet.getInt(4);
                String sex = resultSet.getString(5);
                Sex gender = null;
                if (sex.equalsIgnoreCase("male")) {
                    gender = Sex.MALE;
                } else {
                    gender = Sex.FEMALE;
                }
                Person person = new Person(name, surname, bornYear, phone, gender);
                personsList.add(person);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally{
            try {
                resultSet.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        request.setAttribute("personsList", personsList);
        request.getRequestDispatcher("showPersons.jsp").forward(request,response);
    }
}
