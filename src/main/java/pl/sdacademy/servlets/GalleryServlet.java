package pl.sdacademy.servlets;

import pl.sdacademy.model.Gallery;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@WebServlet(name = "GalleryServlet", value = "/gallery")
public class GalleryServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Gallery catsGallery = new Gallery("Obrazki słodkich kotków");
        String filePath = "C:\\Users\\Krzysztof\\Desktop\\JAVA OD PODSTAW\\JSP\\src\\main\\resources\\imagesFile";
        System.out.println(filePath);
        FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;

        while ((line = bufferedReader.readLine()) != null) {
            String[] split = line.split("   @");
            catsGallery.addImage(split[0],split[1]);
        }
        fileReader.close();
        bufferedReader.close();

        System.out.println("Gallery name:" + catsGallery.getName());
        System.out.println("Gallery date:" + catsGallery.getDate());
        System.out.println("Gallery images:" + catsGallery.getImages());

        request.setAttribute("my_gallery", catsGallery);
// ta metoda powoduje ze nie tylko wyswietlamy, ale tez przekazujemy obiekty Java
        request.getRequestDispatcher("gallery.jsp").forward(request, response);
    }
}
