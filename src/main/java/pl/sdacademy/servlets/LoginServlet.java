//package pl.sdacademy.servlets;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@WebServlet(name = "LoginServlet", value = "/login")
//public class LoginServlet extends HttpServlet {
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        final String login = "Krzysiek";
//        final String password = "Krzysiek";
//
//        String inputLogin = request.getParameter("login");
//        String inputPassword = request.getParameter("password");
//
//        if ((inputLogin.equals(login)) && (inputPassword.equals(password))) {
//            Cookie cookie = new Cookie("user", inputLogin);
//            cookie.setMaxAge(60);
//            response.addCookie(cookie);
//            response.sendRedirect("index.jsp");
//        } else {
//            String errorMessage = "Invalid login or password";
//            request.setAttribute("errorMessage", errorMessage);
//            request.getRequestDispatcher("login.jsp").forward(request, response);
//        }
//    }
//
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.sendRedirect("login.jsp");
//    }
//}
