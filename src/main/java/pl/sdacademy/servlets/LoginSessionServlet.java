package pl.sdacademy.servlets;

import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "LoginServlet", value = "/login")
public class LoginSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String login = "Krzysiek";
        final String password = "Krzysiek";

        String inputLogin = request.getParameter("login");
        String inputPassword = DigestUtils.sha512Hex(request.getParameter("password"));

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:C:/Users/Krzysztof/Desktop/JAVA OD PODSTAW/SQLite/users.db");
            preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE login LIKE '" + inputLogin + "' AND password LIKE '" + inputPassword + "'");
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next() == true) {
                HttpSession session = request.getSession(); // zwraca nowy obiekt sesji
                session.setAttribute("user", inputLogin);
                session.setMaxInactiveInterval(60);
                response.sendRedirect("index.jsp");
            } else {
                String errorMessage = "Invalid login or password";
                request.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }
}
