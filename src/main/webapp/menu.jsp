<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<div>
    <h2>Login:</h2>
    <ul>
        <c:choose>
            <c:when test="${sessionScope.user == null}">
                <li><a href="login">Login</a></li>
                <li><a href="registration">Registration</a></li>
            </c:when>
            <c:when test="${sessionScope.user != null}">
                <li><a href="logout">Logout</a></li>
                <h1 style="color: greenyellow">Hi you are logged as ${sessionScope.user}!</h1>
            </c:when>
        </c:choose>
    </ul>
    <h2>Change password</h2>
    <ul>
        <li><a href="changePassword">Change password</a></li>
    </ul>
    <h2>Menu:</h2>
    <ul>
        <li><a href="home?action=index">Home</a></li>
        <li><a href="gallery">Gallery</a></li>
        <li><a href="about">About</a></li>
        <li><a href="contact">Contact</a></li>
        <li><a href="instagram">Instagram</a></li>
        <li><a href="showPersons">Show persons</a></li>
        <li><a href="addPerson">Add person</a></li>
        <li><a href="museums">Museums</a></li>
    </ul>
</div>