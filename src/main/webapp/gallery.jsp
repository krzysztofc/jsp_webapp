<%@ page import="pl.sdacademy.model.Gallery" %>
<%@ page import="pl.sdacademy.model.Image" %><%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-01
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Gallery</title>
</head>
<body>

<h1>GALLERY</h1>

<jsp:include page="menu.jsp"/>
<div>
    <% Gallery gallery = (Gallery) request.getAttribute("my_gallery"); %>
    <h4>
        Name: <%= gallery.getName() %>
    </h4>
    <h4>
        Date: <%= gallery.getDate() %>
    </h4>
</div>
<div>

    <table border="3px" width="25%">
        <% for(Image image : gallery.getImages()) { %>
        <tr>
            <td>
                <img src="<%= image.getImageUrl()%>" width="100%" title="<%= image.getDescription()%>"/>
            </td>
        </tr>
        <% } %>
    </table>

</div>

</body>
</html>
