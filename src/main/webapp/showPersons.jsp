<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-05
  Time: 20:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Show Persons</title>
</head>
<body>
<h1>Persons</h1>
<jsp:include page="menu.jsp"/>

<c:set var="personsList" value="${requestScope.personsList}" scope="request"/>
<h1>Lista osob zawiera: ${personsList.size()}</h1>

<table border="1px">

    <tr>
        <th>
            Name
        </th>
        <th>
            Surname
        </th>
        <th>
            Born year
        </th>
        <th>
            Phone number
        </th>
        <th>
            Sex
        </th>
    </tr>
    <c:forEach items="${personsList}" var="person">
        <tr>
            <td><c:out value="${person.getName()}"/></td>
            <td><c:out value="${person.getLastName()}"/></td>
            <td><c:out value="${person.getBornYear()}"/></td>
            <td><c:out value="${person.getPhoneNumber()}"/></td>
            <td><c:out value="${person.getSex()}"/></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
