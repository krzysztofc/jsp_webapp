<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-08
  Time: 17:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Person</title>
</head>
<body>
<h1>ADD PERSON</h1>
<jsp:include page="menu.jsp"/>

<div>
    <form method="post" action="/addPerson">
        <table>
            <tr>
                <td>
                    Enter your name:
                </td>
                <td>
                    <input type="text" size="40" id="name" name="name">
                </td>
            </tr>
            <tr>
                <td>
                    Enter your surname:
                </td>
                <td>
                    <input type="text" size="40" id="surname" name="surname">
                </td>
            </tr>
            <tr>
                <td>
                    Enter your born year:
                </td>
                <td>
                    <input type="text" size="40" id="bornYear" name="bornYear">
                </td>
            </tr>
            <tr>
                <td>
                    Enter your phone number:
                </td>
                <td>
                    <input type="text" size="40" id="phoneNumber" name="phone">
                </td>
            </tr>
            <tr>
                <td>
                    Enter your sex:
                </td>
                <td>
                    <select name="sex">
                        <option value="MALE" name="sex">MALE</option>
                        <option value="FEMALE" name="sex">FEMALE</option>
                    </select>
                </td>
            </tr>
        </table>
        <input type="submit" value="Add person">
    </form>
</div>



</body>
</html>
