<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>

<h1><% out.print("HOME!"); %></h1>

<div>
    <h2 style="color: green"><c:out value="${registrationMsg}"/></h2>
    <h2 style="color: green"><c:out value="${changedPassword}"/></h2>
    <jsp:include page="menu.jsp"/>
</div>

</body>
</html>