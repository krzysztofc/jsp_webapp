<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-01
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <title>About</title>
</head>
<body>
<h1>ABOUT</h1>

<jsp:include page="menu.jsp"/>
<c:set var="persons" value="${requestScope.persons}" scope="request"/>
<c:set var="womanFirst" value="${requestScope.womanFirst}" scope="request"/>
<h1>Nasza lista osob zawiera: ${persons.size()}</h1>

<table border="1px">

    <tr>
        <th>
            Name
        </th>
        <th>
            Surname
        </th>
        <th>
            Born year
        </th>
        <th>
            Phone number
        </th>
        <th>
            Sex
        </th>
    </tr>
    <c:forEach items="${persons}" var="person">
        <tr>
            <td><c:out value="${person.getName()}"/></td>
            <td><c:out value="${person.getLastName()}"/></td>
            <td><c:out value="${person.getBornYear()}"/></td>
            <td><c:out value="${person.getPhoneNumber()}"/></td>
            <td><c:out value="${person.getSex()}"/></td>
        </tr>
    </c:forEach>
</table>
<br><br><br><br>
<table border="3px">

    <tr>
        <th>
            Name
        </th>
        <th>
            Surname
        </th>
        <th>
            Born year
        </th>
        <th>
            Phone number
        </th>
        <th>
            Sex
        </th>
    </tr>
    <c:forEach items="${womanFirst}" var="person">
        <tr>
            <td><c:out value="${person.getName()}"/></td>
            <td><c:out value="${person.getLastName()}"/></td>
            <td><c:out value="${person.getBornYear()}"/></td>
            <td><c:out value="${person.getPhoneNumber()}"/></td>
            <td><c:out value="${person.getSex()}"/></td>
        </tr>
    </c:forEach>
</table>




</body>
</html>
