<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-11
  Time: 18:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<h1>REGISTRATION</h1>
<h2 style="color: red"><c:out value="${errorMsg}"/></h2>
<form method="post" action="/registration">
    <table>
        <tr>
            <td><label for="login">Login</label></td>
            <td>
                <input type="text" id="login" name="login"/>
            </td>
        </tr>
        <br>
        <tr>
            <td><label for="password">Password</label></td>
            <td>
                <input type="password" id="password" name="password"/>
            </td>
            <td>
                <input type="submit" value="Add user"/>
            </td>
    </table>
</form>
<%--<jsp:include page="menu.jsp"/>--%>
</body>
</html>
