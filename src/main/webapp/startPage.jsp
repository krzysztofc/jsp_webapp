<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-12
  Time: 20:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Start page</title>
</head>
<body>

<h1>START PAGE</h1>

<h2>Welcome to java11ldz JSP portal!</h2>
<h2>Please log in or register if you don't have have an account.</h2>

<ul>
    <li><a href="login">Login</a></li>
    <li><a href="registration">Registration</a></li>
</ul>

</body>
</html>
