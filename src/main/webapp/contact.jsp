<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-01
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Contact</title>
</head>
<body>
<h1>CONTACT</h1>
<jsp:include page="menu.jsp"/>

<div>
    <form method="post" action="/contact">
       <table>
           <tr>
               <td>
                   Enter your name:
               </td>
               <td>
                   <input type="text" size="40" id="name" name="name">
               </td>
           </tr>
           <tr>
               <td>
                   Enter your surname:
               </td>
               <td>
                   <input type="text" size="40" id="surname" name="surname">
               </td>
           </tr>
           <tr>
               <td>
                   Enter your Email:
               </td>
               <td>
                   <input type="email" size="40" id="email" name="email">
               </td>
           </tr>
           <tr>
               <td>
                   Enter your message:
               </td>
               <td>
                   <textarea type="text" size="400" cols="70" rows="15" id="message" name="message"></textarea>
               </td>
           </tr>
       </table>
        <input type="submit" value="Send message">
    </form>
</div>


</body>
</html>
