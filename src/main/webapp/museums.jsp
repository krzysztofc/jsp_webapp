<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-08
  Time: 20:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Museums</title>
</head>
<body style="background: #89cff0">

<c:set var="museumsList" value="${requestScope.museumsList}" scope="request"/>
<h1>MUSEUMS</h1>
<jsp:include page="menu.jsp"/>
<br><br>
<div align="center">
    <form action="/museums" method="post">
        <table style="background: blue">
            <tr>
                <th>
                    Szukaj:
                </th>
                <th>
                    <select name="select">
                        <option value="id">Id</option>
                        <option value="name">Name</option>
                        <option value="postalCode">Postal code</option>
                        <option value="city">City</option>
                        <option value="streetPrefix">Street prefix</option>
                        <option value="street">Street</option>
                        <option value="houseNumber">House number</option>
                        <option value="flatNumber">Flat number</option>
                        <option value="organizer">Organizer</option>
                        <option value="entryDate">Entry date</option>
                        <option value="status">Status</option>
                    </select>
                </th>
                <th>
                    <input type="text" size="100" id="value" name="inputText">
                </th>
                <th>
                    <input type="submit" value="Search">
                </th>
            </tr>
        </table>
    </form>
</div>
<br>
<br>
<div align="center">
    <table method="post" action="/museums" border="2px" width="95%">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Postal code</th>
            <th>City</th>
            <th>Street prefix</th>
            <th>Street</th>
            <th>House Number</th>
            <th>Flat Number</th>
            <th>Organizer</th>
            <th>Entry Date</th>
            <th>Status</th>
        </tr>
        <c:forEach items="${museumsList}" var="museum">
            <tr>
                <td><c:out value="${museum.getId()}"/></td>
                <td><c:out value="${museum.getName()}"/></td>
                <td><c:out value="${museum.getPostalCode()}"/></td>
                <td><c:out value="${museum.getCity()}"/></td>
                <td><c:out value="${museum.getStreetPrefix()}"/></td>
                <td><c:out value="${museum.getStreet()}"/></td>
                <td><c:out value="${museum.getHouseNumber()}"/></td>
                <td><c:out value="${museum.getFlatNumber()}"/></td>
                <td><c:out value="${museum.getOrganizer()}"/></td>
                <td><c:out value="${museum.getEntryDate()}"/></td>
                <td><c:out value="${museum.getStatus()}"/></td>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>
