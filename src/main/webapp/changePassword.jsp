<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-11
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Change password</title>
</head>
<body>
<h1>CHANGE PASSWORD</h1>
<h2 style="color: red"><c:out value="${errorMsg}"/></h2>
<h2 style="color: green"><c:out value="${changedPassword}"/></h2>
<form method="post" action="/changePassword">
    <table>
        <tr>
            <td><label for="login">Login</label></td>
            <td>
                <input type="text" id="login" name="login"/>
            </td>
        </tr>
        <br>
        <tr>
            <td><label for="password">Password</label></td>
            <td>
                <input type="password" id="password" name="password"/>
            </td>
        <tr>
            <td><label for="password">New password</label></td>
            <td>
                <input type="password" id="newPassword" name="newPassword"/>
            </td>
            <td><label for="password">Repeat new password</label></td>
            <td>
                <input type="password" id="newPassword2" name="newPassword2"/>
            </td>
            <td>
                <input type="submit" value="Change password"/>
            </td>
        </tr>


    </table>
</form>
<jsp:include page="menu.jsp"/>
</body>
</html>
