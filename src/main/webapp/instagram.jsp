<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-04
  Time: 20:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Instagram</title>
</head>
<body>

<h1>Instagram form</h1>
<jsp:include page="menu.jsp"/>

<form method="post" action="/instagram">
    <table>
        <tr>
            <td>Instagram username:</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>Number of photos:</td>
            <td><input type="text" name="number"></td>
        </tr>
    </table>
    <input type="submit" value="Send">
</form>

</body>
</html>
