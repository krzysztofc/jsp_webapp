<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: Krzysztof
  Date: 2018-10-04
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SendMessage</title>
</head>

<body>
<h1>YOUR FORM DETAILS</h1>
<jsp:include page="menu.jsp"/>

<h3>Name:${requestScope.name}</h3>
<h3>Surname:${requestScope.surname}</h3>
<h3>Email:${requestScope.email}</h3>
<h3>Message:${requestScope.message}</h3>

</body>

</html>
